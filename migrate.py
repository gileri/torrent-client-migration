#!/bin/env python3

import argparse
import logging
import os
import typing

import qbittorrentapi
import xmlrpc.client


def get_all_torrents(server: str) -> typing.Iterator[typing.Tuple[str, str]]:
    with xmlrpc.client.ServerProxy(server) as proxy:
        for h in proxy.download_list():
            yield (h, proxy.d.directory(h))


def migrate(**s: dict) -> None:
    qbt = qbittorrentapi.Client(
        host=s["qbt_host"],
        port=s["qbt_port"],
        username=s["qbt_user"],
        password=s["qbt_pass"],
    )
    for h, path in get_all_torrents(
        f"{s['rt_proto']}://{s['rt_user']}:{s['rt_pass']}@{s['rt_host']}:{s['rt_port']}{s['rt_path']}"
    ):
        magnet = f"magnet:?xt=urn:btih:{h}"
        logging.info("%s will be downloaded to %s", h, path)
        if not s["dry_run"]:
            qbt.torrents.add(
                urls=(magnet,),
                save_path=path,
                # RTorrent returns the whole path, including root folder
                is_root_folder=False,
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''Migrate torrents from rTorrent to qBittorrent.
                                     Each parameter can be passed as an environment variable,
                                     e.g. QBT_HOST=localhost for --qbt-host''')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='Do not add torrents, just tell what is going to be done')
    parser.add_argument('--qbt-host',
                        default=os.environ.get("QBT_HOST", False),
                        help='qBittorrent hostname')
    parser.add_argument('--qbt-port',
                        default=os.environ.get("QBT_PORT", None),
                        help='qBittorrent port')
    parser.add_argument('--qbt-user',
                        default=os.environ.get("QBT_USER", None),
                        help='qBittorrent hostname')
    parser.add_argument('--qbt-pass',
                        default=os.environ.get("QBT_PASS", None),
                        help='qBittorrent password')
    parser.add_argument('--rt-proto',
                        default=os.environ.get("RT_PROTO", "http"),
                        help='rtorrent XML RPC protocol such as https')
    parser.add_argument('--rt-host',
                        default=os.environ.get("RT_HOST", "localhost"),
                        help='rtorrent XML RPC hostname')
    parser.add_argument('--rt-port',
                        default=os.environ.get("RT_PORT", "80"),
                        help='rtorrent XML RPC port')
    parser.add_argument('--rt-user',
                        default=os.environ.get("RT_USER", None),
                        help='rTorrent hostname')
    parser.add_argument('--rt-pass',
                        default=os.environ.get("RT_PASSWORD", None),
                        help='rTorrent password')
    parser.add_argument('--rt-path',
                        default=os.environ.get("path", "/RPC2"),
                        help='rTorrent path to the XMLRPC endpoint. Default /RPC2')

    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    migrate(**vars(args))
